package com.kader.bazid.springbootreactintegration.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// import lombok.extern.slf4j.Slf4j;

/**
 * WelcomeController
 */

@RestController
// @Slf4j
public class WelcomeController {

    @GetMapping("/hello")
    public String homeScreen() {
        // log.info();
        return "hello";
    }

}