package com.kader.bazid.springbootreactintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootReactIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootReactIntegrationApplication.class, args);
	}

}
